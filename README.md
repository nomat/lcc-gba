仓库地址:[**lcc-gba**](https://gitee.com/nomat/lcc-gba "lcc-gba")

# lcc-gba
Cocos Creator 嵌入GBA模拟器插件。

## 前言
前段时间做了一个FC模拟器的插件[**lcc-nes**](https://gitee.com/nomat/lcc-nes "lcc-nes")，又在网上找到了js实现的GBA模拟器[**gbajs**](https://github.com/endrift/gbajs.git "gbajs")，既然做了，就做全面点吧，所以就有了这个GBA模拟器的插件。同样当前插件只有在web平台有声音。
`插件的模拟器部分使用typescript整理过，并不是使用的原来github上面的js代码`

## 安装
安装十分简单，只要把这个项目作为Creator插件放到插件目录就可以了,具体请查看[**Creator插件包**](https://docs.cocos.com/creator/manual/zh/extension/your-first-extension.html "Creator插件包")。</br>

## 使用
使用也很简单，所有组件在`LCC GBA组件`组里面，如下图:
</br>![avatar](https://gitee.com/nomat/lcc-gba/raw/master/docs/QQ%E6%88%AA%E5%9B%BE20201111114527.jpg)</br>
### 第一步，先添加`Emulator`组件到节点上。
如下图:
</br>![avatar](https://gitee.com/nomat/lcc-gba/raw/master/docs/QQ截图20201111114902.jpg)</br>
图片里面有个`火焰纹章-封印之剑.gba`的二进制文件，这是插件里面测试用的，你可以在插件`roms`目录里面找到，这个目录可能后面会删除。所以`ROM`可以自己下载，因为creator二进制文件必须是.bin结尾的，所以你应该需要修改文件后缀名，测试rom位置如下图:
</br>![avatar](https://gitee.com/nomat/lcc-gba/raw/master/docs/QQ截图20201111114957.jpg)</br>
`Bios`可以是默认的，好像这个一般不需要改动。
### 第二步，添加显示对象。
`Emulator`组件有`getTexture()` 函数可以获得渲染的纹理，你可以用在需要的地方。插件也提供`Sprite`组件的显示方式，可以在节点上添加`DisplaySprites`组件，然后把`Sprite`组件拖到里面的数组中。如下图:
</br>![avatar](https://gitee.com/nomat/lcc-gba/raw/master/docs/QQ截图20201111114912.jpg)</br>
现在应该就可以看到显示的内容了，大概如下图:
</br>![avatar](https://gitee.com/nomat/lcc-gba/raw/master/docs/QQ截图20201111115336.jpg)</br>

### 第三步，添加手柄控制器。
把`Controller`组件添加到里面去，里面可以选择玩家的数量，目前jsgba支持2个玩家，然后就可以修改按键映射。如下图:
</br>![avatar](https://gitee.com/nomat/lcc-gba/raw/master/docs/QQ截图20201111114922.jpg)</br>
你也可以在代码里面控制，可以直接看源码：
```
// 常量定义，注意模块嵌套
module lcc.gba {

/**
 * 游戏按钮
 */

export enum Button {
	A = 0,
	B = 1,
	SELECT = 2,
	START = 3,
	RIGHT = 4,
	LEFT = 5,
	UP = 6,
	DOWN = 7,
	R = 8,
	L = 9,
}

}
```
* 通过节点事件控制游戏: gba_button_event 
```
// 游戏按钮A， 按下
this.emit("gba_button_event", lcc.gba.Button.A, true);

// 游戏按钮A， 放开
this.emit("gba_button_event", lcc.gba.Button.A, false);

// 注意: 虽然是直接通过代码控制，但是控制器必须存在。也就是说必须有Controller在节点上。
```
* 也可以直接获取`Controller`组件，调用`onButtonEvent(lcc.gba.Button.A, true)`这种方式实现。

## 更多功能
比如一些`自定义存储`等功能，请看源码。`core`目录中的代码时整理后的gba模拟器ts实现。

# 结束语
不管能不能用上，先储备起来。。。

